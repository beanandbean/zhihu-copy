import path from "path";
import webpack from "webpack";
import CopyPlugin from "copy-webpack-plugin";
import { merge } from "webpack-merge";

const env =
  process.env["NODE_ENV"] === "production" ? "production" : "development";

const src = path.join(__dirname, "src");

const config: webpack.Configuration = {
  mode: env,
  entry: {
    content: path.join(src, "content/index.ts"),
    background: path.join(src, "background/index.ts"),
    "background-wrapper": path.join(src, "background-wrapper.ts"),
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: (pathData: any) => {
      if (pathData.chunk.name === "background-wrapper") {
        return "[name].js";
      } else {
        return "js/[name].js";
      }
    },
  },
  optimization: {
    splitChunks: {
      name: "vendor",
      chunks: "initial",
    },
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: "raw" }],
    }),
  ],
};
export default env === "production"
  ? config
  : merge(config, {
      devtool: "inline-source-map",
    });
