import { Content, GET_SOURCE_REQUEST } from "../utils";

let data: Content | null = null;

const testRichContent = (element: HTMLElement) => {
  return (
    element.tagName === "DIV" &&
    element.classList.contains("RichContent") &&
    !element.classList.contains("is-collapsed")
  );
};

const testQuestionRichText = (element: HTMLElement) => {
  return (
    element.tagName === "DIV" &&
    element.classList.contains("QuestionRichText") &&
    !element.classList.contains("QuestionRichText--collapsed")
  );
};

const testRichText = (element: HTMLElement) => {
  return (
    element.tagName === "DIV" &&
    element.classList.contains("RichText") &&
    !element.classList.contains("PinItem-remainContentRichText")
  );
};

document.addEventListener("contextmenu", (event) => {
  data = null;

  let element = event.target as HTMLElement | null;
  while (
    element !== null &&
    !testRichContent(element) &&
    !testQuestionRichText(element) &&
    !testRichText(element)
  ) {
    element = element.parentElement;
  }
  if (element !== null) {
    let type: "qa" | "column" = testRichText(element) ? "column" : "qa";

    const clone = element.cloneNode(true) as HTMLElement;
    // Remove dynamic content
    for (const dynamicClass of [
      "Voters",
      "Reward",
      "Sticky--holder",
      "ContentItem-time",
      "ContentItem-actions",
      "AnswerItem-editButton",
      "LinkCard-desc",
    ]) {
      for (const elem of Array.from(
        clone.getElementsByClassName(dynamicClass)
      )) {
        if (elem.parentElement !== null) {
          elem.parentElement.removeChild(elem);
        }
      }
    }

    for (const elem of Array.from(
      clone.getElementsByClassName("Image-Wrapper-Preview")
    )) {
      if (elem.parentElement !== null) {
        const parent = elem.parentElement;
        parent.removeChild(elem);
        for (const img of Array.from(
          elem.getElementsByClassName("Image-Preview")
        )) {
          if (img.tagName === "IMG") {
            const i = img as HTMLImageElement;
            i.className = "content_image";
            i.removeAttribute("height");

            const actualSrc = img.attributes.getNamedItem("data-actualsrc");
            if (actualSrc !== null) {
              i.src = actualSrc.value;
            }
            const rawWidth = img.attributes.getNamedItem("data-rawwidth");
            if (rawWidth !== null) {
              i.width = parseInt(rawWidth.value);
            } else {
              i.removeAttribute("width");
            }

            const figure = document.createElement("figure");
            figure.appendChild(i);
            parent.appendChild(figure);
          }
        }
      }
    }

    data = { html: clone.outerHTML, type };
  }
});

chrome.runtime.onMessage.addListener((request, _, respond) => {
  if (request === GET_SOURCE_REQUEST) {
    if (data !== null) {
      respond({ data, error: false });
    } else {
      respond({ data: {}, error: true });
    }
  }
});
