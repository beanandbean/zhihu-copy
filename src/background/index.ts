import { Content, GET_SOURCE_REQUEST } from "../utils";

type SourceResponse = {
  data: Content;
  error: boolean;
};

const sites = ["*://*.zhihu.com/*"];

const cache = "https://beanandbean.gitlab.io/zhihu-copy/20210827/";
const css = {
  qa: [
    cache + "main.app.216a26f4.7f81711cd0d07df8b4d2.css",
    cache + "main.common.216a26f4.a91f28f26db4378c475f.css",
  ],
  column: [cache + "column.app.216a26f4.fe08bf6d005b6465cef1.css"],
};

const styleInject = `
div.RichContent, div.QuestionRichText, div.RichText {
  background: #fff;
  padding: 20px;
}
`;

chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id: "extract",
    title: "提取内容到新标签页",
    contexts: ["page", "selection", "image", "link"],
    documentUrlPatterns: sites,
  });
});
chrome.contextMenus.onClicked.addListener((info, tab) => {
  if (info.menuItemId === "extract") {
    if (tab !== undefined && tab.id !== undefined) {
      chrome.tabs.sendMessage(
        tab.id,
        GET_SOURCE_REQUEST,
        { frameId: info.frameId },
        (data: SourceResponse) => {
          console.log(data);
          if (!data.error) {
            let output = "";
            for (const file of css[data.data.type]) {
              output += `<link rel="stylesheet" type="text/css" href="${file}" />\n`;
            }
            output += `<style>${styleInject}</style>\n\n`;
            output += data.data.html;
            chrome.tabs.create({
              url: "data:text/html;charset=utf-8," + encodeURIComponent(output),
            });
          }
        }
      );
    }
  }
});
