export const GET_SOURCE_REQUEST = "getSource";

export type Content = {
  html: string;
  type: "qa" | "column";
};
